### A Pluto.jl notebook ###
# v0.12.4

using Markdown
using InteractiveUtils

# ╔═╡ d7a2b82a-150a-11eb-3a61-73780141b71e
begin
	using DynamicPolynomials, LinearAlgebra
	import Base.Iterators: flatten
end

# ╔═╡ b4118aca-1551-11eb-377a-3f9427166b2d
md"## Baker-Campbell-Hausdorff - Some Verification"

# ╔═╡ 7c94a2a8-154e-11eb-1d52-89b9a6ca5d5f
md"Import things we need."

# ╔═╡ 85e26744-154e-11eb-2400-f77f3eb36c53
md"Declare $x, y$ as non commutative variables."

# ╔═╡ eca42722-150a-11eb-00a1-83eee2cd9453
@ncpolyvar x y;

# ╔═╡ 9708a1c8-154e-11eb-12b4-7be92bb4f157
md"This is a trick to keep track of powers of $x$ and $y$ only up to the order `ord`.

The declared taylor number can be tought of as a generalization of the dual numbers to higher order using their matrix representations.
"

# ╔═╡ f51a0c8c-150a-11eb-2bdb-1925a48280f3
taylor_number(s, ord) = diagm(1 => repeat([s], ord - 1));

# ╔═╡ 10bc5eb0-154f-11eb-1410-d52053193889
md"The following will calculate with order up to but not including 10."

# ╔═╡ abb613e0-150c-11eb-2ee4-13a17060c8db
order = 10

# ╔═╡ 2c2ed6be-154f-11eb-0b32-d7983036c5f8
md"Declare taylor number for $x$."

# ╔═╡ 2863a328-150b-11eb-23ab-b789b2eed1e3
X = taylor_number(x, order)

# ╔═╡ 5e9f36d6-154f-11eb-04e8-d5be7a0b3c01
md"To illustrate the usefulness of the here introduced taylor numbers, consider the square of $X$. It is evident that not only does the matrix now contain $x^2$ instead of $x$ but it also has been shifted up one row."

# ╔═╡ 1cf9006c-1521-11eb-3858-a513f1a982cd
X^2

# ╔═╡ e1b12c1e-154f-11eb-0f2c-d114126524a1
md"The higher the exponent, the further the contens of $X$ are shifted up..."

# ╔═╡ 21ad8b1e-1521-11eb-29b2-d588c15fb575
X^7

# ╔═╡ 0746fcc2-1550-11eb-3add-99e27b9a13a3
md"... and finally disapper with order 10."

# ╔═╡ 0168c86c-1550-11eb-3d6c-f73639e51d4d
X^10

# ╔═╡ 191c5816-1550-11eb-3714-2f5eaa5d2151
md"Declare another taylor number for $y$."

# ╔═╡ 5f81d0f0-150b-11eb-1bbf-158a81e65df1
Y = taylor_number(y, order)

# ╔═╡ 34b04628-1550-11eb-060e-af64bc24ffbb
md"Multiplying two taylor numbers in different variables works as well and the upward shift is the sum of the exponents of $x$ and $y$."

# ╔═╡ 323f4c04-1521-11eb-08c7-e55c2cba551e
Y^2*X

# ╔═╡ b446f88a-1550-11eb-0f49-cf0e21fb7171
md"Series expansion of $e^x$ up to the order of the taylor number given as input..."

# ╔═╡ 6b437b82-150b-11eb-240e-bfcaa699eede
exp_ser(A) = sum(1//factorial(n) * A^n for n in 0:size(A, 1)-1);

# ╔═╡ 19413e46-1551-11eb-3581-dba15999b386
md"... and that one for $\ln$ too."

# ╔═╡ d4563416-150b-11eb-0d8d-39d5e623a8dc
ln_ser(A) = sum((-1)^(n-1)//n * (A - I)^n for n in 1:size(A, 1)-1);

# ╔═╡ 2f435ad0-1551-11eb-2ec4-2dc3a6c56484
md"Calculate the first `order` terms of the series for $\ln(e^X \cdot e^Y)$.

The actual result can be read from the first row of the matrix representating the taylor number, which contains the terms of the final sum sorted by their polynomial degree."

# ╔═╡ b01f7558-150b-11eb-1cca-0562d52d29e1
my_res = ln_ser(exp_ser(X)*exp_ser(Y))[1, :];

# ╔═╡ 8b5f00da-1551-11eb-2d6f-31ae02c5f97d
md"The terms for higher order get quite long and are cut of."

# ╔═╡ f262d6de-150c-11eb-1d90-71190e098a66
join(my_res[1:6].|>t->"- $t", "\n")|>Markdown.parse

# ╔═╡ ec015b02-1551-11eb-1409-b5216721c61e
md"Now that we have some results, lets verify they are correct!"

# ╔═╡ d9571b44-1551-11eb-3e4d-ef5a060c1882
md"### Comparison to results from [Wikipedia](https://en.wikipedia.org/wiki/Baker%E2%80%93Campbell%E2%80%93Hausdorff_formula)"

# ╔═╡ 05ce3eee-1552-11eb-2e17-599bb7208187
md"Define the commutator `c(x,y)`=$[x, y] = xy - yx$ and a shorthand for nested commutators like `c(x,c(y,c(x,y))) = c(x*y*x*y)`."

# ╔═╡ 06c65b5e-150e-11eb-3635-a94d820769ab
begin
	c(x, y) = x*y - y*x
	c(a::AbstractArray) = foldr(c, a)
	c(m::Monomial) = c(collect(flatten(repeat([s], n) for (s, n) in  zip(variables(m), exponents(m)))))
end;

# ╔═╡ cf4dd89c-1552-11eb-0aa7-23f38aad225b
md"Now expand the commutators given in the formula from Wikipedia and put the resulting terms in a list ordered by their degree."

# ╔═╡ cc217a3c-150e-11eb-0b4c-9301ff7592e9
wikipedia_res = [
	0,
	x+y,
	1//2*c(x,y),
	1//12*(c(x*x*y) + c(y*y*x)),
	-1//24*c(y*x*x*y),
	-1//720*(c(y*y*y*y*x) + c(x*x*x*x*y)) + 1//360*(c(x*y*y*y*x) + c(y*x*x*x*y)) + 1//120*(c(y*x*y*x*y) + c(x*y*x*y*x))
];

# ╔═╡ 8c5ad188-1553-11eb-3273-a99dc0c3b5b8
md"The terms look very similar to the ones we already calculated and stored in `my_res`..."

# ╔═╡ 29eb3f9e-151f-11eb-00dc-cb0c08830c2d
join(wikipedia_res.|>t->"- $t", "\n")|>Markdown.parse

# ╔═╡ a2b15588-1553-11eb-23d1-f3639eecfc21
md"... and in fact are equal!"

# ╔═╡ 1af6b62c-151e-11eb-29c0-8daf086a01d7
my_res[1:6] .== wikipedia_res

# ╔═╡ ee33e902-1554-11eb-3e48-e33fd9f787f8
begin
	res_str = join(enumerate(my_res).|>((n, t),)->"#### Order $(n-1):\n$t", "\n\n---\n\n")
	replace("And finally all terms up to order $order in all their glory:

$(res_str)", "//"=>"/")|>Markdown.parse
end

# ╔═╡ Cell order:
# ╟─b4118aca-1551-11eb-377a-3f9427166b2d
# ╟─7c94a2a8-154e-11eb-1d52-89b9a6ca5d5f
# ╠═d7a2b82a-150a-11eb-3a61-73780141b71e
# ╟─85e26744-154e-11eb-2400-f77f3eb36c53
# ╠═eca42722-150a-11eb-00a1-83eee2cd9453
# ╟─9708a1c8-154e-11eb-12b4-7be92bb4f157
# ╠═f51a0c8c-150a-11eb-2bdb-1925a48280f3
# ╟─10bc5eb0-154f-11eb-1410-d52053193889
# ╠═abb613e0-150c-11eb-2ee4-13a17060c8db
# ╟─2c2ed6be-154f-11eb-0b32-d7983036c5f8
# ╠═2863a328-150b-11eb-23ab-b789b2eed1e3
# ╟─5e9f36d6-154f-11eb-04e8-d5be7a0b3c01
# ╠═1cf9006c-1521-11eb-3858-a513f1a982cd
# ╟─e1b12c1e-154f-11eb-0f2c-d114126524a1
# ╠═21ad8b1e-1521-11eb-29b2-d588c15fb575
# ╟─0746fcc2-1550-11eb-3add-99e27b9a13a3
# ╠═0168c86c-1550-11eb-3d6c-f73639e51d4d
# ╟─191c5816-1550-11eb-3714-2f5eaa5d2151
# ╠═5f81d0f0-150b-11eb-1bbf-158a81e65df1
# ╟─34b04628-1550-11eb-060e-af64bc24ffbb
# ╠═323f4c04-1521-11eb-08c7-e55c2cba551e
# ╟─b446f88a-1550-11eb-0f49-cf0e21fb7171
# ╠═6b437b82-150b-11eb-240e-bfcaa699eede
# ╟─19413e46-1551-11eb-3581-dba15999b386
# ╠═d4563416-150b-11eb-0d8d-39d5e623a8dc
# ╟─2f435ad0-1551-11eb-2ec4-2dc3a6c56484
# ╠═b01f7558-150b-11eb-1cca-0562d52d29e1
# ╟─8b5f00da-1551-11eb-2d6f-31ae02c5f97d
# ╠═f262d6de-150c-11eb-1d90-71190e098a66
# ╟─ec015b02-1551-11eb-1409-b5216721c61e
# ╟─d9571b44-1551-11eb-3e4d-ef5a060c1882
# ╟─05ce3eee-1552-11eb-2e17-599bb7208187
# ╠═06c65b5e-150e-11eb-3635-a94d820769ab
# ╟─cf4dd89c-1552-11eb-0aa7-23f38aad225b
# ╠═cc217a3c-150e-11eb-0b4c-9301ff7592e9
# ╟─8c5ad188-1553-11eb-3273-a99dc0c3b5b8
# ╠═29eb3f9e-151f-11eb-00dc-cb0c08830c2d
# ╟─a2b15588-1553-11eb-23d1-f3639eecfc21
# ╠═1af6b62c-151e-11eb-29c0-8daf086a01d7
# ╟─ee33e902-1554-11eb-3e48-e33fd9f787f8
